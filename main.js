const app = new Vue({

    el: '#app',
    data: {
        opcion: 0,
        Mensaje: ''
    },
    methods: {
        miFuncion() {

            var ale = Math.round(Math.random() * (4 - 1) + 1)

            var compu = ""
            var tu = ""

            if (this.opcion == 1) {
                tu = "piedra"
            } else if (this.opcion == 2) {
                tu = "papel"
            } else if (this.opcion == 3) {
                tu = "tijera"
            }

            if (ale == 1) {
                compu = "piedra"
            } else if (ale == 2) {
                compu = "papel"
            } else if (ale == 3) {
                compu = "tijera"
            }

            if (compu == "piedra" && tu == "papel") {
                this.Mensaje = "Ganastes!"
            } else if (compu == "papel" && tu == "tijera") {
                this.Mensaje = "Ganastes!"
            } else if (compu == "tijera" && tu == "piedra") {
                this.Mensaje = "Ganastes!"
            } else if (compu == "papel" && tu == "piedra") {
                this.Mensaje = "Perdistes!"
            } else if (compu == "tijera" && tu == "papel") {
                this.Mensaje = "Perdistes!"
            } else if (compu == "piedra" && tu == "tijera") {
                this.Mensaje = "Perdistes!"
            } else if (compu == tu) {
                this.Mensaje = "Empate!"
            }

        }

    }
})